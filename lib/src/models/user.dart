import 'package:tinder_fake/src/models/id.dart';
import 'package:tinder_fake/src/models/location.dart';
import 'package:tinder_fake/src/models/login.dart';
import 'package:tinder_fake/src/models/name.dart';
import 'package:tinder_fake/src/models/picture.dart';

class User {
  String gender;
  Name name;
  Location location;
  String email;
  Login login;
  String dob;
  String registered;
  String phone;
  String cell;
  Id id;
  Picture picture;
  String nat;


  User( this.name, this.location, this.registered, this.phone,
      this.cell, this.picture);

  User.fromJsonMap(Map<String, dynamic> map)
      : gender = map["gender"],
        name = Name.fromJsonMap(map["name"]),
        location = Location.fromJsonMap(map["location"]),
        email = map["email"],
        login = Login.fromJsonMap(map["login"]),
        dob = map["dob"],
        registered = map["registered"],
        phone = map["phone"],
        cell = map["cell"],
        id = Id.fromJsonMap(map["id"]),
        picture = Picture.fromJsonMap(map["picture"]),
        nat = map["nat"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['gender'] = gender;
    data['name'] = name == null ? null : name.toJson();
    data['location'] = location == null ? null : location.toJson();
    data['email'] = email;
    data['login'] = login == null ? null : login.toJson();
    data['dob'] = dob;
    data['registered'] = registered;
    data['phone'] = phone;
    data['cell'] = cell;
    data['id'] = id == null ? null : id.toJson();
    data['picture'] = picture == null ? null : picture.toJson();
    data['nat'] = nat;
    return data;
  }
}
