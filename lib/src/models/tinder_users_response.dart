import 'package:tinder_fake/src/models/user.dart';
import 'package:tinder_fake/src/models/info.dart';

class TinderUsersResponse {

  List<User> results;
  Info info;

	TinderUsersResponse(this.results, this.info);

	TinderUsersResponse.fromJsonMap(Map<String, dynamic> map):
		results = List<User>.from(map["results"].map((it) => User.fromJsonMap(it))),
		info = Info.fromJsonMap(map["info"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['results'] = results != null ? 
			this.results.map((v) => v.toJson()).toList()
			: null;
		data['info'] = info == null ? null : info.toJson();
		return data;
	}
}
