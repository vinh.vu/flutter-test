import 'package:rxdart/rxdart.dart';
import 'package:tinder_fake/src/models/tinder_users_response.dart';
import 'package:tinder_fake/src/models/user.dart';
import 'package:tinder_fake/src/resources/users_repository.dart';

class HomePageBloc {
  HomePageBloc(this._repository);

  UsersRepository _repository;
  final _usersFetcher = PublishSubject<List<User>>();
  final _usersFavouriteFetcher = PublishSubject<List<User>>();

  Observable<List<User>> get usersTinder => _usersFetcher.stream;
  Observable<List<User>> get usersFavourite => _usersFavouriteFetcher.stream;

  fetchTinderUsers() async {
    TinderUsersResponse usersResponse = await _repository.fetchUsers();
    _usersFetcher.sink.add(usersResponse.results);
  }

  storeUsersFavourite(User user) async {
    await _repository.storeUsersFavourite(user);
  }

  fetchUsersFavourite() async {
    List<User> users = await _repository.fetchUsersFavourite();
    _usersFavouriteFetcher.sink.add(users);
  }

  dispose() {
    _usersFetcher.close();
    _usersFavouriteFetcher.close();
  }
}

final homePageBloc = HomePageBloc(UsersRepository());
