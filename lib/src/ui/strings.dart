class Strings {
  static String myInfo = "My info is";
  static String myAddress = "My address is";
  static String myPhone = "My phone is";
  static String mySchedule = "My schedule is";
  static String myStatus = "My status is";
  static String empty = "";
  static String cantGetUserList="Can not get user list";
  static String addedToFavourite="Added to favourite";
}
