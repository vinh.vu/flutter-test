import 'package:flutter/material.dart';

class DrawCircle extends CustomPainter {
  Paint _paint;
  Paint _paintFill;
  double _radius;

  DrawCircle(double radius) {
    this._radius = radius;
    _paint = Paint()
      ..color = Color.fromARGB(100, 209, 209, 209)
      ..strokeWidth = 2.0
      ..style = PaintingStyle.stroke;

    _paintFill = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(0.0, 0.0), _radius, _paintFill);
    canvas.drawCircle(Offset(0.0, 0.0), _radius, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
