import 'package:flutter/material.dart';
import 'package:tinder_fake/src/blocs/home_page_bloc.dart';
import 'package:tinder_fake/src/models/user.dart';
import 'package:tinder_fake/src/ui/home/drawer.dart';

import '../draw_circle.dart';
import '../strings.dart';

class HomePage extends StatefulWidget {
  HomePage({this.bloc});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final HomePageBloc bloc;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _index = 0;
  var scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    widget.bloc.fetchTinderUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.black12,
        drawer: HomeDrawer(widget.bloc),
        body: homeBody());
  }

  Widget homeBody() {
    return StreamBuilder(
      stream: widget.bloc.usersTinder,
      builder: (context, AsyncSnapshot<List<User>> snapshot) {
        if (snapshot.hasData) {
          return Center(
            child: userList(snapshot),
          );
        } else if (snapshot.hasError) {
          return Center(child: Text(snapshot.error.toString()));
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }

  Widget userList(AsyncSnapshot<List<User>> snapshot) {
    return Dismissible(
        resizeDuration: null,
        onDismissed: (DismissDirection direction) {
          if (direction.index == DismissDirection.startToEnd.index) {
            showSnackBar(Strings.addedToFavourite);
            widget.bloc
                .storeUsersFavourite(getUserByIndex(_index, snapshot.data));
          }
          setState(() {
            _index += 1;
          });
        },
        key: ValueKey(_index),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
                width: 350,
                height: 400,
                child: Stack(
                  children: <Widget>[
                    Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8))),
                    Container(
                        height: 150,
                        decoration: BoxDecoration(
                            color: Color.fromARGB(100, 232, 232, 232),
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(8.0),
                                topLeft: Radius.circular(8.0)))),
                    Positioned(
                      top: 150,
                      child: Container(
                        width: 350,
                        height: 2,
                        color: Color.fromARGB(100, 209, 209, 209),
                      ),
                    )
                  ],
                )),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 150,
                  height: 150,
                  child: Center(
                    child: Stack(
                      children: <Widget>[
                        Container(
                            child: Center(
                          child: CustomPaint(painter: DrawCircle(85)),
                        )),
                       Container(
                                width: 150.0,
                                height: 150.0,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    image: DecorationImage(
                                        fit: BoxFit.fill,
                                        image: getUserByIndex(_index, snapshot.data).picture.large != null ?
                                        NetworkImage(getUserByIndex(_index, snapshot.data).picture.large):
                                        AssetImage("assets/images/default_avatar.png"))))
                      ],
                    ),
                  ),
                ),
                BottomBar(getUserByIndex(_index, snapshot.data))
              ],
            )
          ],
        ));
  }

  void showSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(message)));
  }

  User getUserByIndex(int index, List<User> users) {
    return users[_index % users.length];
  }
}

class BottomBar extends StatefulWidget {
  BottomBar(this.user);

  final User user;

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _selectedIndex = 0;
  String title;
  String info;

  void _onItemTapped(int index) {
    _selectedIndex = index;
    setState(() {
      setData();
    });
  }

  @override
  void initState() {
    super.initState();
    buildBottomNavigationBarItems();
    setData();
  }

  void setData() {
    switch (Tab.values[_selectedIndex]) {
      case Tab.User:
        title = Strings.myInfo;
        info = "${widget.user.name.title}: ${widget.user.name.first} ${widget.user.name.last}";
        break;
      case Tab.Schedule:
        title = Strings.mySchedule;
        info = widget.user.registered;
        break;
      case Tab.Address:
        title = Strings.myAddress;
        info = widget.user.location.street;
        break;
      case Tab.Phone:
        title = Strings.myPhone;
        info = widget.user.phone;
        break;
      case Tab.Lock:
        title = Strings.myStatus;
        info = widget.user.cell;
        break;
      default:
        title = Strings.empty;
        info = Strings.empty;
    }
  }

  List<BottomNavigationBarItem> bottomNavigationBarItems = List();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      width: 300,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(title, style: TextStyle(color: Colors.grey, fontSize: 15)),
              Text(info,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                  ),
                  textAlign: TextAlign.center)
            ],
          ),
        ),
        bottomNavigationBar: Container(
          margin: EdgeInsets.only(left: 50, top: 0, right: 50, bottom: 0),
          child: BottomNavigationBar(
            backgroundColor: Colors.transparent,
            type: BottomNavigationBarType.fixed,
            items: bottomNavigationBarItems,
            elevation: 0,
            currentIndex: _selectedIndex,
            selectedItemColor: Colors.greenAccent,
            unselectedItemColor: Colors.grey,
            onTap: _onItemTapped,
          ),
        ),
      ),
    );
  }

  void buildBottomNavigationBarItems() {
    bottomNavigationBarItems.clear();
    Tab.values.forEach((tab) {
      bottomNavigationBarItems.add(BottomNavigationBarItem(
          icon: Icon(iconData(tab)),
          backgroundColor: Colors.transparent,
          title: Padding(padding: EdgeInsets.all(0))));
    });
  }

  IconData iconData(Tab tab) {
    switch (tab) {
      case Tab.User:
        return Icons.person;
      case Tab.Schedule:
        return Icons.schedule;
      case Tab.Address:
        return Icons.map;
      case Tab.Phone:
        return Icons.phone;
      case Tab.Lock:
        return Icons.lock;
      default:
        return Icons.home;
    }
  }
}

enum Tab { User, Schedule, Address, Phone, Lock }
