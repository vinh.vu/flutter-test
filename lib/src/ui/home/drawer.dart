import 'package:flutter/material.dart';
import 'package:tinder_fake/src/blocs/home_page_bloc.dart';
import 'package:tinder_fake/src/models/user.dart';
import 'package:tinder_fake/src/ui/strings.dart';

class HomeDrawer extends StatefulWidget {
  HomePageBloc bloc;

  HomeDrawer(this.bloc);

  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.bloc.fetchUsersFavourite();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.white,
      child: StreamBuilder(
        stream: widget.bloc.usersFavourite,
        builder: (context, AsyncSnapshot<List<User>> snapshot) {
          if (snapshot.hasData) {
            return Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 50, top: 80, bottom: 50),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Favourite users",
                        style: TextStyle(
                            color: Colors.greenAccent,
                            fontSize: 30,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.only(
                          left: 50,
                          right: 50,
                          top: 25,
                          bottom:25
                        ),
                        height: 100,
                        child: Stack(
                          children: <Widget>[
                            Container(
                                width: 100.0,
                                height: 100.0,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    image: DecorationImage(
                                        fit: BoxFit.fill,
                                        image: snapshot.data[index].picture
                                                    .large !=
                                                null
                                            ? NetworkImage(snapshot
                                                .data[index].picture.large)
                                            : AssetImage(
                                                "assets/images/default_avatar.png")))),
                            Positioned(
                              top: 20,
                              left: 120,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("${snapshot.data[index].name.title}: ${snapshot.data[index].name.first} ${snapshot.data[index].name.last}"),
                                  Text("${Strings.myPhone}: ${snapshot.data[index].phone}"),
                                  Text("${Strings.myAddress}: ${snapshot.data[index].location.street}"),
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                )
              ],
            );
          } else if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
