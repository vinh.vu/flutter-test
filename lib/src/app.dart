import 'package:flutter/material.dart';
import 'package:tinder_fake/src/ui/home/home_page.dart';

import 'blocs/home_page_bloc.dart';

class TinderFake extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: HomePage(bloc: homePageBloc),
    );
  }
}
