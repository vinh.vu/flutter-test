import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tinder_fake/src/models/user.dart';

class SharedPreferencesUtil {
  final String KEY_USER_FAVOURITE = "KEY_USER_FAVOURITE";

  Future<List<User>> fetchUsersFavourite() async {
    var sharedPreferences = await SharedPreferences.getInstance();
    var usersFavouriteInJson = sharedPreferences.getString(KEY_USER_FAVOURITE);
    if (usersFavouriteInJson != null) {
      Iterable iterable = jsonDecode(usersFavouriteInJson);
      List<User> users =
          iterable.map((model) => User.fromJsonMap(model)).toList();
      return users;
    }
    return List<User>();
  }

  storeUserFavourite(User user) async {
    var sharedPreferences = await SharedPreferences.getInstance();
    await fetchUsersFavourite().then((users) {
      print(isUserHaveBeenStored(users, user));
      if (isUserHaveBeenStored(users, user) == false) {
        users.add(user);
        sharedPreferences.setString(KEY_USER_FAVOURITE, jsonEncode(users));
      }
    });
  }

  bool isUserHaveBeenStored(List<User> users, User user) {
    var stored = false;
    users.forEach((userStored) {
      if (userStored.id.value == user.id.value) {
        stored = true;
      }
    });
    return stored;
  }
}
