import 'package:tinder_fake/src/models/tinder_users_response.dart';
import 'package:tinder_fake/src/models/user.dart';
import 'package:tinder_fake/src/utils/json_loader.dart';
import 'package:tinder_fake/src/utils/shared_preferences_util.dart';

class UserProvider {
//  Client client = Client();
  SharedPreferencesUtil sharedPreferencesUtil;

  UserProvider() {
    sharedPreferencesUtil = SharedPreferencesUtil();
  }

  Future<TinderUsersResponse> fetchTinderUsers() async {
//    final response = await client.get("https://randomuser.me/api/0.4/?randomapi");
//    if (response.statusCode == 200) {
//      return TinderUsersResponse.fromJsonMap(json.decode(response.body));
//    } else {
////      load local
    final usersTinder = await parseJsonFromAssets("assets/tinder_user.json");
    return TinderUsersResponse.fromJsonMap(usersTinder);
//      throw Exception('Failed to load post');
//    }
  }

  Future<List<User>> fetchUsersFavourite() async {
    return sharedPreferencesUtil.fetchUsersFavourite();
  }

  storeUserFavourite(User user) async {
    sharedPreferencesUtil.storeUserFavourite(user);
  }
}
