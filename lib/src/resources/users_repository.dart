import 'package:tinder_fake/src/models/tinder_users_response.dart';
import 'package:tinder_fake/src/models/user.dart';
import 'package:tinder_fake/src/resources/tinder_user_provider.dart';

class UsersRepository {
  final usersProvider = UserProvider();

  Future<TinderUsersResponse> fetchUsers() => usersProvider.fetchTinderUsers();

  Future<List<User>> fetchUsersFavourite() => usersProvider.fetchUsersFavourite();

  storeUsersFavourite(User user) => usersProvider.storeUserFavourite(user);

}
