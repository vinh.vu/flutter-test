import 'package:mockito/mockito.dart';
import 'package:tinder_fake/src/resources/users_repository.dart';

class MockUsersRepository extends Mock implements UsersRepository {}
