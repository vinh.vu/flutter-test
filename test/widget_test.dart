// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:tinder_fake/src/blocs/home_page_bloc.dart';
import 'package:tinder_fake/src/models/info.dart';
import 'package:tinder_fake/src/models/location.dart';
import 'package:tinder_fake/src/models/name.dart';
import 'package:tinder_fake/src/models/picture.dart';
import 'package:tinder_fake/src/models/tinder_users_response.dart';
import 'package:tinder_fake/src/models/user.dart';
import 'package:tinder_fake/src/resources/users_repository.dart';
import 'package:tinder_fake/src/ui/home/drawer.dart';
import 'package:tinder_fake/src/ui/home/home_page.dart';

import 'mock_users_repository.dart';

void main() {
  HomePageBloc homeBloc;
  UsersRepository userRepository;

  setUp(() {
    userRepository = MockUsersRepository();
    homeBloc = HomePageBloc(userRepository);
  });
  test("fetch Users Favourite", () async {
    final users = List<User>();
    when(userRepository.fetchUsersFavourite())
        .thenAnswer(((response) async => users));
    expect(userRepository.fetchUsersFavourite(),
        isInstanceOf<Future<List<User>>>());
  });

  test("fetch Users Tinder", () async {
    final users = List<User>();
    when(userRepository.fetchUsers()).thenAnswer(((response) async => TinderUsersResponse(users, Info())));
    final response = await userRepository.fetchUsers();
    expect(userRepository.fetchUsers(), isInstanceOf<Future<TinderUsersResponse>>());
    expect(response.results.length, 0);
  });

  testWidgets('test widgets users tinder', (WidgetTester tester) async {
    final users = List<User>();
    users.add(User(Name("Miss", "Vinh", "Vu"), Location("HCM"), "registered", "0987654321", "0987654321", Picture(null)));
    when(userRepository.fetchUsers()).thenAnswer(((response) async => TinderUsersResponse(users, Info())));
    await tester.pumpWidget(MaterialApp(home: HomePage(bloc: homeBloc)));
    expect(find.byType(CircularProgressIndicator), findsOneWidget);
    await tester.pumpAndSettle();
    expect(find.byType(Dismissible), findsOneWidget);
  });

   testWidgets('test widgets Users favourite', (WidgetTester tester) async {
    final users = List<User>();
    users.add(User(Name("Miss", "Vinh", "Vu"), Location("HCM"), "registered", "0987654321", "0987654321", Picture(null)));
    when(userRepository.fetchUsersFavourite()).thenAnswer(((response) async => users));
    await tester.pumpWidget(MaterialApp(home: HomeDrawer(homeBloc)));
    expect(find.byType(HomeDrawer), findsOneWidget);
    await tester.pumpAndSettle();
    expect(find.byType(ListView), findsOneWidget);
  });

}
